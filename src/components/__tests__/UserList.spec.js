import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import UserList from '../UserList.vue'

describe('UserList', () => {
  it('renders properly', () => {
    const wrapper = mount(UserList, { props: { apiURL: 'https://reqres.in/api/users' } })
    expect(wrapper.text()).toContain('https')
  })
})
